import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { RouterModule, Routes } from '@angular/router'
import { EffectsModule } from '@ngrx/effects'
import {
  RouterStateSerializer,
  StoreRouterConnectingModule,
} from '@ngrx/router-store'
import { MetaReducer, StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { NxModule } from '@nrwl/nx'
import { FeatureMapModule } from '@ui/feature/map'
import { FeaturePanelOneModule } from '@ui/feature/panel-one'
import { UiCharteModule } from '@ui/ui/charte'
import { storeFreeze } from 'ngrx-store-freeze'

import { environment } from '../environments/environment'

import { AppComponent } from './app.component'
import { PageOneComponent } from './presentation/page/page-one/page-one.component'

import { CustomSerializer, effects, reducers } from './store'
import { UiLayoutModule } from '@ui/ui/layout'

export const metaReducers: MetaReducer<any>[] = !environment.production
  ? [storeFreeze]
  : []

export const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'page-one',
  },
  {
    path: 'page-one',
    component: PageOneComponent,
  },
]

@NgModule({
  declarations: [AppComponent, PageOneComponent],
  imports: [
    BrowserModule,
    NxModule.forRoot(),
    RouterModule.forRoot(ROUTES, {
      initialNavigation: 'enabled',
    }),
    FeatureMapModule,
    FeaturePanelOneModule,
    UiCharteModule,
    UiLayoutModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
  ],
  providers: [{ provide: RouterStateSerializer, useClass: CustomSerializer }],
  bootstrap: [AppComponent],
})
export class AppModule {}
