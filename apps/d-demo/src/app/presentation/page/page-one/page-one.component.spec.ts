import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { EffectsModule } from '@ngrx/effects'
import { StoreModule } from '@ngrx/store'
import { NxModule } from '@nrwl/nx'
import { FeatureMapModule } from '@ui/feature/map'
import { FeaturePanelOneModule } from '@ui/feature/panel-one'

import { PageOneComponent } from './page-one.component'
import { NO_ERRORS_SCHEMA } from '@angular/core'

describe('PageOneComponent', () => {
  let component: PageOneComponent
  let fixture: ComponentFixture<PageOneComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PageOneComponent],
      imports: [
        FeatureMapModule,
        FeaturePanelOneModule,
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PageOneComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
