import { HttpClientTestingModule } from '@angular/common/http/testing'
import { TestBed } from '@angular/core/testing'
import { NavigationExtras, Router } from '@angular/router'
import { provideMockActions } from '@ngrx/effects/testing'
import { cold, hot } from 'jasmine-marbles'
import { Observable } from 'rxjs'
import { Location } from '@angular/common'

import * as fromActions from '../actions/'

import * as fromEffects from './router.effect'
import createSpyObj = jasmine.createSpyObj

describe('RouterEffects', () => {
  let router: Router
  let location: Location
  let effects: fromEffects.RouterEffects
  let actions: Observable<any>

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        fromEffects.RouterEffects,
        provideMockActions(() => actions),
        {
          provide: Router,
          useValue: createSpyObj('Router', ['navigate']),
        },
        {
          provide: Location,
          useValue: createSpyObj('Location', ['back', 'forward']),
        },
      ],
    })

    effects = TestBed.get(fromEffects.RouterEffects)
    router = TestBed.get(Router)
    location = TestBed.get(Location)
  })

  describe('navigate$', () => {
    it('should call router navigate', () => {
      const payload: any = {
        path: ['.'],
        query: {
          id: 10,
        },
        extras: {
          queryParamsHandling: 'merge',
        },
      }
      const action = new fromActions.Go(payload)

      actions = hot('--a', { a: action })
      const expected = cold('--b', { b: action })

      effects.navigate$.subscribe(() => {
        const { path, query: queryParams, extras } = payload
        expect(router.navigate).toHaveBeenCalledWith(path, {
          queryParams,
          ...extras,
        })
      })
    })
  })

  describe('navigateBack$', () => {
    it('should call location back', () => {
      const action = new fromActions.Back()

      actions = hot('-a', { a: action })
      const expected = cold('-b', { b: action })

      effects.navigate$.subscribe(() => {
        expect(location.back).toHaveBeenCalled()
      })
    })
  })

  describe('navigateForward$', () => {
    it('should call location forward', () => {
      const action = new fromActions.Back()

      actions = hot('-a', { a: action })
      const expected = cold('-b', { b: action })

      effects.navigate$.subscribe(() => {
        expect(location.forward).toHaveBeenCalled()
      })
    })
  })
})
