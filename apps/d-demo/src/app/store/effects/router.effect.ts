import { Location } from '@angular/common'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, Effect, ofType } from '@ngrx/effects'
import { map, tap } from 'rxjs/operators'

import * as RouterActions from '../actions/router.action'

@Injectable()
export class RouterEffects {
  constructor(
    private _actions$: Actions,
    private _router: Router,
    private _location: Location
  ) {}

  @Effect({ dispatch: false })
  navigate$ = this._actions$.pipe(ofType(RouterActions.GO)).pipe(
    map((action: RouterActions.Go) => action.payload),
    tap(({ path, query: queryParams, extras }) => {
      this._router.navigate(path, { queryParams, ...extras })
    })
  )

  @Effect({ dispatch: false })
  navigateBack$ = this._actions$
    .pipe(ofType(RouterActions.BACK))
    .pipe(tap(() => this._location.back()))

  @Effect({ dispatch: false })
  navigateForward$ = this._actions$
    .pipe(ofType(RouterActions.FORWARD))
    .pipe(tap(() => this._location.forward()))
}
