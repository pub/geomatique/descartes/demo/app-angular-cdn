import { TestBed } from '@angular/core/testing'
import { Store, StoreModule } from '@ngrx/store'
import * as fromReducers from '../reducers/index'
import { RouterStateUrl } from '../reducers/index'
import * as fromSelectors from '../selectors/router.selectors'

describe('Router Selectors', () => {
  let store: Store<fromReducers.RouterStateUrl>

  const state: RouterStateUrl = {
    url: 'this-is-url',
    queryParams: {
      param1: 'val1',
      param2: 'val2',
    },
    params: {
      param1: 'val11',
      param2: 'val22',
    },
  }

  const routerStateRoot = { state }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot(fromReducers.reducers)],
    })
    store = TestBed.get(Store)
  })

  describe('getQueryParams', () => {
    it('should return state of router store slice', () => {
      expect(fromSelectors.getQueryParams.projector(routerStateRoot)).toBe(
        state.queryParams
      )
    })
  })

  describe('getFullUrl', () => {
    it('should return state of router store slice', () => {
      expect(fromSelectors.getFullUrl.projector(routerStateRoot)).toBe(
        `${location.origin}this-is-url`
      )
    })
  })
})
