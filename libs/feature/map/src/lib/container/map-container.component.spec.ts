import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { MapContainerComponent } from './map-container.component'
import { NxModule } from '@nrwl/nx'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

describe('MapContainerComponent', () => {
  let component: MapContainerComponent
  let fixture: ComponentFixture<MapContainerComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
      ],
      declarations: [MapContainerComponent],
      providers: [],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(MapContainerComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
