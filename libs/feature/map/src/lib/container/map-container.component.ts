import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ViewEncapsulation,
} from '@angular/core'

import { select, Store } from '@ngrx/store'
import * as fromStore from '../store'

@Component({
  selector: 'ui-map-container',
  templateUrl: './map-container.component.html',
  styleUrls: ['./map-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MapContainerComponent implements OnInit {
  map: any
  descartesMap: any
  view: any
  coucheOSM: any
  @ViewChild('map') map_el: ElementRef
  @ViewChild('toolbarZoom') toolbarZoom: ElementRef
  @ViewChild('toolbarView') toolbarView: ElementRef
  @ViewChild('toolbarMeasure') toolbarMeasure: ElementRef
  @ViewChild('toolbarMisc') toolbarMisc: ElementRef
  @ViewChild('footer') footer_el: ElementRef
  @ViewChild('descartesScaleSelector') descartesScaleSelector: ElementRef
  @ViewChild('descartesGraphicScale') descartesGraphicScale: ElementRef
  @ViewChild('descartesMetricScale') descartesMetricScale: ElementRef
  @ViewChild('descartesMousePosition') descartesMousePosition: ElementRef
  @ViewChild('descartesMiniMap') descartesMiniMap: ElementRef

  constructor(private _store: Store<fromStore.MapState>) {}

  ngOnInit() {

    const extent = [
      -800086,
      5055726,
      1150172,
      6772047
    ]

    this.view = new ol.View()

    const content = new Descartes.MapContent({displayIconLoading:false})
    
    this.coucheOSM = new Descartes.Layer.OSM("couche OSM préconfigurée")
	content.addItem(this.coucheOSM)
	
    this.descartesMap = new Descartes.Map.ContinuousScalesMap(
      this.map_el.nativeElement,
      content,
      {
        projection: 'EPSG:3857',
        initExtent: extent,
        maxExtent: extent,
        minScale: 6500000,
        autoScreenSize: true,
        autoSize: true,
        displayExtendedOLExtent: true
      }
    )
    
    this.descartesMap.addContentManager("descartesLayersTree")
    
    this.descartesMap.show()

    this.map = this.descartesMap.OL_map

    this._bindMapToState()

    this.initMapWidgets()

    // Initial view
    this._store.dispatch(
      new fromStore.SetView({
        center: [328627.563458, 5921296.662223],
        zoom: 0,
      })
    )

    // start loop to update map size
    this._refreshMapSize()
  }

  _bindMapToState(): void {
    this._store
      .pipe(select(fromStore.getMapCenter))
      .subscribe((center: number[]) => {
        this.map.getView().setCenter(center)
      })

    this._store.pipe(select(fromStore.getMapZoom)).subscribe((zoom: number) => {
      this.map.getView().setZoom(zoom)
    })
  }

  _refreshMapSize() {
    this.map.updateSize()
    window.requestAnimationFrame(this._refreshMapSize.bind(this))
  }

  initMapWidgets() {
    const toolbarView = this.descartesMap.addNamedToolBar(
      this.toolbarView.nativeElement,
      null,
      { vertical: true }
    )
    this.descartesMap.addToolInNamedToolBar(toolbarView, {
      type: Descartes.Map.ZOOM_IN,
    })
    this.descartesMap.addToolInNamedToolBar(toolbarView, {
      type: Descartes.Map.MAXIMAL_EXTENT,
    })
    this.descartesMap.addToolInNamedToolBar(toolbarView, {
      type: Descartes.Map.NAV_HISTORY,
    })

    const toolbarMeasure = this.descartesMap.addNamedToolBar(
      this.toolbarMeasure.nativeElement,
      null,
      { vertical: true }
    )
    this.descartesMap.addToolInNamedToolBar(toolbarMeasure, {
      type: Descartes.Map.AREA_MEASURE,
    })
    this.descartesMap.addToolInNamedToolBar(toolbarMeasure, {
      type: Descartes.Map.DISTANCE_MEASURE,
    })

    this.descartesMap.addInfo({
      type: Descartes.Map.MOUSE_POSITION_INFO,
      div: "descartesMousePosition",
      options: {separator: '-', prefix: 'Position du curseur : ', displayProjections: ["EPSG:3857","EPSG:4326","EPSG:4326-DM","EPSG:4326-DMS"], inline:true}
    })
    this.descartesMap.addInfo({
      type: Descartes.Map.GRAPHIC_SCALE_INFO,
      div: "descartesGraphicScale",
    })
    this.descartesMap.addInfo({
      type: Descartes.Map.METRIC_SCALE_INFO,
      div: "descartesMetricScale",
    })
    
    
    this.descartesMap.addAction({
      type: Descartes.Map.SCALE_SELECTOR_ACTION,
      div: "descartesScaleSelector",
      options: {
        optionsPanel: {collapsible: true, collapsed: false}
      }
    })

    this.descartesMap.addOpenLayersControls([
      {
        type: Descartes.Map.OL_ZOOM,
        args: {
          target: this.toolbarZoom.nativeElement,
          className: 'map-tools-zoom',
        },
      },
      {
        type: Descartes.Map.OL_FULL_SCREEN,
        args: {
          source: document.querySelector('ui-page-one'),
          target: this.toolbarMisc.nativeElement,
          className: 'map-tools-fullscreen',
        },
      },
      {
        type: Descartes.Map.OL_OVERVIEW_MAP,
        args: {
          target: "descartesMiniMap",
          layers: [
            this.coucheOSM.OL_layers[0]
          ],
          collapseLabel: "Masquer l'aperçu",
          label: "Afficher l'aperçu",
          className: 'map-tools-overviewmap',
        },
      },
    ])
    this.descartesMap.addOpenLayersInteractions([
      { type: Descartes.Map.OL_DRAG_ROTATE },
      { type: Descartes.Map.OL_DRAG_PAN, args:{condition:ol.events.condition.noModifierKeys} },
      { type: Descartes.Map.OL_DRAG_ZOOM },
      { type: Descartes.Map.OL_DOUBLE_CLICK_ZOOM },
      { type: Descartes.Map.OL_MOUSE_WHEEL_ZOOM },
      { type: Descartes.Map.OL_PINCH_ZOOM },
      { type: Descartes.Map.OL_PINCH_ROTATE }
    ])
  }
}
