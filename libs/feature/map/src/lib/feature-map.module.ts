import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { MapContainerComponent } from './container/map-container.component'
import { StoreModule } from '@ngrx/store'
import { initialState, MAP_FEATURE_KEY, reducer } from './store/reducers'

@NgModule({
  declarations: [MapContainerComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(MAP_FEATURE_KEY, reducer, {
      initialState,
    }),
  ],
  exports: [MapContainerComponent],
})
export class FeatureMapModule {}
