import * as fromActions from './map.actions'
import { View } from '../../models/view'

describe('Map Actions', () => {
  describe('SetView Action', () => {
    it('should create an action', () => {
      const payload: View = {
        center: [0, 0],
        zoom: 2,
      }
      const action = new fromActions.SetView(payload)

      expect({ ...action }).toEqual({
        payload,
        type: fromActions.SET_VIEW,
      })
    })
  })

  describe('SetView Action', () => {
    it('should create an action', () => {
      const payload = 2
      const action = new fromActions.SetZoom(payload)

      expect({ ...action }).toEqual({
        payload,
        type: fromActions.SET_ZOOM,
      })
    })
  })
})
