import * as fromMap from '../actions'
import { View } from '../../models/view'

export interface MapState {
  center: number[]
  zoom: number
}

export const initialState: MapState = {
  center: [0, 0],
  zoom: 2,
}

export function reducer(
  state = initialState,
  action: fromMap.MapActions
): MapState {
  switch (action.type) {
    case fromMap.SET_VIEW: {
      const view: View = action.payload
      return {
        ...state,
        ...view,
      }
    }
    case fromMap.SET_ZOOM: {
      const zoom: number = action.payload
      return {
        ...state,
        zoom,
      }
    }
  }

  return state
}

export const getCenter = (state: MapState) => state && state.center
export const getZoom = (state: MapState) => state && state.zoom
