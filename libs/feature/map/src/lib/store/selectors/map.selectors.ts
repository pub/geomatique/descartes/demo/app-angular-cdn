import { createFeatureSelector, createSelector } from '@ngrx/store'
import * as fromMap from '../reducers/map.reducer'
import { MAP_FEATURE_KEY } from '../reducers'

export const getMapState = createFeatureSelector<fromMap.MapState>(
  MAP_FEATURE_KEY
)

export const getMapCenter = createSelector(
  getMapState,
  fromMap.getCenter
)

export const getMapZoom = createSelector(
  getMapState,
  fromMap.getZoom
)

export const getMapView = createSelector(
  getMapState,
  getMapCenter,
  getMapZoom,
  (state: fromMap.MapState, center: number[], zoom: number) => {
    return { center, zoom }
  }
)
