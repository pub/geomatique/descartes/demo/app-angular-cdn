import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { PanelOneContainerComponent } from './panel-one-container.component'
import { NxModule } from '@nrwl/nx'
import { StoreModule } from '@ngrx/store'
import { EffectsModule } from '@ngrx/effects'

describe('PanelOneContainerComponent', () => {
  let component: PanelOneContainerComponent
  let fixture: ComponentFixture<PanelOneContainerComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NxModule.forRoot(),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
      ],
      declarations: [PanelOneContainerComponent],
      providers: [],
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelOneContainerComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
