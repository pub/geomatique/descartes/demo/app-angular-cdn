import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ViewEncapsulation,
} from '@angular/core'


@Component({
  selector: 'ui-panel-one-container',
  templateUrl: './panel-one-container.component.html',
  styleUrls: ['./panel-one-container.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PanelOneContainerComponent implements OnInit {
  descartesMap: any
  @ViewChild('map') map_el: ElementRef

  constructor() {}

  ngOnInit() {

  }
}
