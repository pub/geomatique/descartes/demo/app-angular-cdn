import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { PanelOneContainerComponent } from './container/panel-one-container.component'

@NgModule({
  declarations: [PanelOneContainerComponent],
  imports: [
    CommonModule
  ],
  exports: [PanelOneContainerComponent],
})
export class FeaturePanelOneModule {}
