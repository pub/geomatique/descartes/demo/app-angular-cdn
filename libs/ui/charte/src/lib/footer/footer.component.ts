import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core'

@Component({
  selector: 'ui-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
  constructor() {}

  ngOnInit() {
  
	  // generic expand/collapse
	  Array.from(document.body.querySelectorAll('.toggle')).forEach(el => {
	  	const target = document.body.querySelector(el.getAttribute('data-target'))
		el.addEventListener('click', () => {
		    target.classList.toggle('show')
		  })
	  })
  
  }
}
